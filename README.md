# Say cheese

The service runs as either google cloud functions or as an express app.

## GCP

In order to run the service as cloud functions you need to

- Create a GCP account
- Enable billing
- Enable the cloud functions API
- Install the google cloud SDK
- Create a storage bucket
  - `gsutil mb gs://[YOUR_BUCKET_NAME]`
- Deploy the functions to GCP
  - `gcloud beta functions deploy retrieveGET --runtime nodejs8 --trigger-http --memory 2048MB --set-env-vars BUCKET_NAME=[YOUR_BUCKET_NAME]`
  - `gcloud beta functions deploy snapshotPOST --runtime nodejs8 --trigger-http --memory 2048MB --set-env-vars BUCKET_NAME=[YOUR_BUCKET_NAME]`
- Then you can call the functions with curl
  - `curl https://[YOUR_REGION]-[YOUR_PROJECT_ID].cloudfunctions.net/snapshotPOST -H Content-Type: application/json -d { "urls": [LIST_OF_URLS] }`
  - `curl https://[YOUR_REGION]-[YOUR_PROJECT_ID].cloudfunctions.net/retrieveGET?site=[YOUR_SITE]`

## Locally

Run `yarn && yarn start` in the root folder and open a new terminal in the `client` folder and run `yarn && yarn start` again. A browser should open automatically.
