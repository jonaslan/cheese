const { Storage } = require("@google-cloud/storage");
const path = require("path");
const projectId = process.env.GCP_PROJECT;

const storage = new Storage({
  projectId: projectId
});

const bucketName = process.env.BUCKET_NAME;

const imageDirectory = "screenshots";

function store(results) {
  return Promise.all(
    results.map(elem => {
      const dir = path.join(imageDirectory, encodeURIComponent(elem.url));

      const p = path.join(dir, elem.capturedAt.toString() + ".png");

      return storage
        .bucket(bucketName)
        .file(p)
        .save(elem.image);
    })
  );
}

async function retrieve(site) {
  const dir = path.join(imageDirectory, encodeURIComponent(site));

  const options = {
    // strip leading slash and add trailing
    prefix: dir.replace(/^\//, "") + "/",
    delimiter: "/"
  };

  const [files] = await storage.bucket(bucketName).getFiles(options);
  const filesWithDates = await Promise.all(
    files.map(async f => {
      const resp = await f.getMetadata();

      const updatedAt = resp[0].updated;
      return {
        updatedAt,
        f
      };
    })
  );

  // sort descending and get first
  const latest = filesWithDates.sort((a, b) => b.updatedAt - a.updatedAt)[0];
  if (latest && latest.f) {
    return latest.f.createReadStream();
  } else {
    throw new Error("No files");
  }
}

module.exports = {
  store,
  retrieve
};
