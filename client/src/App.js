import React, { Component } from "react";
import "./App.css";

class App extends Component {
  constructor() {
    super();
    this.state = {
      queryUrl: "",
      image: "",
      urls: [
        {
          id: 1,
          url: ""
        },
        {
          id: 2,
          url: ""
        },
        {
          id: 3,
          url: ""
        }
      ]
    };
  }
  postUrls = event => {
    event.preventDefault();
    fetch("http://localhost:3000/screenshot/", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        urls: this.state.urls
          .filter(elem => elem.url !== "")
          .map(elem => elem.url)
      })
    });
  };

  updateUrl = event => {
    event.preventDefault();
    const id = parseInt(event.target.id, 10);
    const newValue = event.target.value;
    this.setState(state => {
      const { urls } = state;
      const index = urls.find(elem => elem.id === id);
      if (index) {
        urls[index.id - 1].url = newValue;
      }
      return { ...state, urls };
    });
  };

  addUrl = () => {
    this.setState(state => {
      const { urls } = state;
      const last = urls[urls.length - 1];
      return { ...state, urls: urls.push({ id: last.id + 1, url: "" }) };
    });
  };

  updateQuery = event => {
    this.setState({ queryUrl: event.target.value });
  };

  getScreenshot = async () => {
    try {
      const resp = await fetch(
        `http://localhost:3000/fetch/?site=${this.state.queryUrl}`
      );
      const blob = await resp.blob();
      const objectURL = URL.createObjectURL(blob);
      this.setState({ image: objectURL });
    } catch (err) {
      console.error(err);
    }
  };

  render() {
    return (
      <div className="App">
        <form
          id="urlsform"
          name="urlsform"
          onSubmit={this.postUrls}
          className="UrlForm"
        >
          {this.state.urls.map(url => (
            <input
              key={url.id}
              id={url.id}
              value={url.url}
              onChange={this.updateUrl}
            />
          ))}
          <button>Say cheese</button>
          <button type="button" onClick={this.addUrl}>
            Add url
          </button>
        </form>

        <input onChange={this.updateQuery} />
        <button type="button" onClick={this.getScreenshot}>
          Get screenshot
        </button>
        <div>
          {this.state.image ? (
            <img src={this.state.image} alt="Screenshot" />
          ) : null}
        </div>
      </div>
    );
  }
}

export default App;
