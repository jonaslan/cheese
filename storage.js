const fs = require("fs");

const fsPromises = fs.promises;
const path = require("path");
const url = require("url");

const imageDirectory = "screenshots";

function store(results) {
  return results.map(async elem => {
    try {
      const dir = path.join(
        __dirname,
        imageDirectory,
        encodeURIComponent(elem.url)
      );

      const p = path.join(dir, elem.capturedAt.toString() + ".png");

      const exists = fs.existsSync(p);

      if (exists) {
        await fsPromises.mkdir(dir, { recursive: true });
      }
      return fsPromises.writeFile(p, elem.image);
    } catch (err) {
      console.error(err);
    }
  });
}

async function retrieve(site) {
  try {
    const dir = path.join(__dirname, imageDirectory, encodeURIComponent(site));
    const files = await fsPromises.readdir(dir);
    const latest = files
      .map(name => name.split(".")[0])
      .sort((a, b) => b - a)[0];
    const p = path.join(dir, latest + ".png");
    return fsPromises.readFile(p);
  } catch (err) {
    console.log(err);
  }
}

module.exports = {
  store,
  retrieve
};
