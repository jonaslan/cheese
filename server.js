const express = require("express");

const { snapshot } = require("./app");
const bodyParser = require("body-parser");
const db = require("./storage");
const cors = require("cors");
const app = express();

const PORT = 3000;

app.use(bodyParser.json());
app.use(cors());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

app.get("/fetch", async (req, res) => {
  try {
    const image = await db.retrieve(req.query.site);
    res.end(image);
  } catch (err) {
    console.log(err);
    res.status(500).end();
  }
});

app.post("/screenshot/", async (req, res) => {
  try {
    const response = await snapshot(req.body.urls);
    await db.store(response);
    res.send("Done!");
  } catch (err) {
    console.error(err);
    res.status(500).end();
  }
});

app.listen(PORT, () => console.log("Server running on port: " + PORT));
