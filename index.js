const { snapshot } = require("./app");
const { store, retrieve } = require("./cloud-storage");

exports.retrieveGET = async (req, res) => {
  try {
    const results = await retrieve(req.query.site);
    if (results) {
      results.pipe(res);
    } else {
      res.end();
    }
  } catch (err) {
    console.log(err);
    res.end();
  }
};

exports.snapshotPOST = async (req, res) => {
  try {
    const results = await snapshot(req.body.urls);
    await store(results);
    res.send("Successfully screenshotted urls");
  } catch (err) {
    console.error("Got error", err);
    res.status(500).send("Function got an error" + err);
  }
};
