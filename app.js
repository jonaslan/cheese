const puppeteer = require("puppeteer");

async function snapshot(urls) {
  const browser = await puppeteer.launch({
    args: ["--no-sandbox"]
  });
  const page = await browser.newPage();

  const results = await Promise.all(
    urls.map(async url => {
      if (!/^http?:\/\//.test(url)) {
        url = "http://" + url;
      }
      await page.goto(url);
      const image = await page.screenshot();
      const date = new Date();
      return {
        image,
        url,
        capturedAt: date.getTime()
      };
    })
  );

  await browser.close();

  return results;
}

module.exports = {
  snapshot
};
